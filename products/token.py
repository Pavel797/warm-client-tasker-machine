from .db import db
from pony.orm import PrimaryKey, Required


class Token(db.Entity):
    id = PrimaryKey(int)
    token = Required(str)
