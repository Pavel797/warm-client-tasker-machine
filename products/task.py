import json

import config
from .db import db
from exceptions.vk_token_invalid import VkTokenInvalid
from pony.orm import PrimaryKey, Required, Optional
from creator.vk_creator import VkMarketsCreator
from numpy import array_split

class SmartTask:

    def __init__(self, type, data, work_method):
        self.type = type
        self.data = data

        self.work = work_method


class VkTaskScan(db.Entity):
    task = Required("Task")
    start = Required(int)
    end = Required(int)

    def decompose(self, num_tasks):
        self.start, self.end = (self.end, self.start) if self.start > self.end else (self.start, self.end)
        step = (self.end - self.start) // num_tasks
        if (self.end - self.start) % num_tasks != 0:
            step += 1

        result = []
        for st in range(self.start, self.end, step):
            result.append(SmartTask(type='scan_vk_markets', data={
                'start': max(1, st),
                'end': self.end + 1 if min(self.end, st + step) == self.end else min(self.end, st + step)
            }, work_method=self.work))

        return (self.end - self.start), result

    """
    vk_creator = VkMarketsCreator(token)
    ids_list = vk_creator.scan_markets_range_ids(self.start, self.end)
    bloks_ids = array_split(ids_list, num_tasks)

    result = []
    for blok_ids in bloks_ids:
        result.append(SmartTask(type=config.TYPE_UPDATE_VK_MARKETS, data={
            'ids': blok_ids
        }, work_method=work))

    return len(ids_list), result

    """

    @staticmethod
    def create(task, start, end):
        if start is None or end is None:
            return None
        return VkTaskScan(task=task, start=start, end=end)

    @staticmethod
    def work(data, access_token, data_helper, progress_queue):
        vk_markets_creator = VkMarketsCreator(access_token)
        valid_ids = vk_markets_creator.scan_markets_range_ids(data['start'], data['end'])
        print("Scan count markets", len(valid_ids))
        for id in valid_ids:
            if progress_queue.empty():
                progress_queue.put(id - data['start'])
            while True:
                try:
                    market = vk_markets_creator.get_markets_list_by_id(id)
                    if market is not None:
                        data_helper.save_markers(market)
                    break
                except VkTokenInvalid as e:
                    access_token = data_helper.mark_token_as_invalid_and_update(e.token)
                    vk_markets_creator = VkMarketsCreator(access_token)

        progress_queue.put(data['end'] - data['start'])


class VkTaskUpdate(db.Entity):
    task = Required("Task")
    ids = Required(str)

    def decompose(self, num_tasks):
        ids_list = json.loads(self.ids)

        bloks_ids = array_split(ids_list, num_tasks)

        result = []
        for blok_ids in bloks_ids:
            result.append(SmartTask(type=config.TYPE_UPDATE_VK_MARKETS, data={
                'ids': blok_ids
            }, work_method=self.work))

        return len(ids_list), result

    @staticmethod
    def work(data, access_token, data_helper, progress_queue):
        vk_markets_creator = VkMarketsCreator(access_token)
        for ind in range(0, data['ids'].size):
            if progress_queue.empty():
                progress_queue.put(ind + 1)

            while True:
                try:
                    market = vk_markets_creator.get_markets_list_by_id(int(data['ids'][ind]))
                    if market is not None:
                        data_helper.save_markers(market)
                    break
                except VkTokenInvalid as e:
                    access_token = data_helper.mark_token_as_invalid_and_update(e.token)
                    vk_markets_creator = VkMarketsCreator(access_token)

        progress_queue.put(data['ids'].size)

    @staticmethod
    def create(task, ids):
        if ids is None:
            return None
        return VkTaskUpdate(task=task, ids=ids)


class Task(db.Entity):
    id = PrimaryKey(int)

    type = Required(str)

    vk_task_scan = Optional(VkTaskScan, cascade_delete=True)
    vk_task_update = Optional(VkTaskUpdate, cascade_delete=True)

    def decompose(self, num_tasks):
        if self.type == config.TYPE_SCAN_VK_MARKETS:
            return self.vk_task_scan.decompose(num_tasks)

        if self.type == config.TYPE_UPDATE_VK_MARKETS:
            return self.vk_task_update.decompose(num_tasks)
