from .db import db
from pony.orm import PrimaryKey, Optional, Required


class Market(db.Entity):
    id = PrimaryKey(int, auto=True)

    vk_club_id = Required(int, size=64, unique=True)
    name = Optional(str, default=None, nullable=True)
    description = Optional(str, default=None, nullable=True)
    country = Optional(str, default=None, nullable=True)
    city = Optional(str, default=None, nullable=True)
    site = Optional(str, default=None, nullable=True)
    photo_200 = Optional(str, default=None, nullable=True)
    activity = Optional(str, default=None, nullable=True)
    products_count = Optional(int, default=None, nullable=True)
    median_price = Optional(int, size=64, default=None, nullable=True)
    average_price = Optional(int, size=64, default=None, nullable=True)
    currency = Optional(str, default=None, nullable=True)
    last_post_date = Optional(int, default=None, nullable=True)
    total_wall_posts_count = Optional(int, default=None, nullable=True)
    wall_type = Optional(int, default=None, nullable=True)

    audience = Optional(str, default=None, nullable=True)  # json
    products_example = Optional(str, default=None, nullable=True)  # json
    contacts = Optional(str, default=None, nullable=True)  # json
    links = Optional(str, default=None, nullable=True)  # json

    def to_dict(self):
        return {
            "vk_club_id": self.vk_club_id,
            "audience": self.audience,
            "name": self.name,
            "description": self.description,
            "activity": self.activity,
            "photo_200": self.photo_200,
            "contacts": self.contacts,
            "site": self.site,
            "links": self.links,
            "total_wall_posts_count": self.total_wall_posts_count,
            "wall_type": self.wall_type,
            "country": self.country,
            "city": self.city,
            "last_post_date": self.last_post_date,
            "products_count": self.products_count,
            "median_price": self.median_price,
            "average_price": self.average_price,
            "products_example": self.products_example,
            "currency": self.currency
        }
