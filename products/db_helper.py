import json

import config
from products.db import db
from pony.orm import select, commit, delete
from products.token import Token
from products.market import Market
from products.task import Task, VkTaskScan, VkTaskUpdate
import logging


def db_init():
    """ Инициализация ORM для локальной БД
    """

    db.bind(provider='sqlite', filename='../db.sqlite', create_db=True)
    db.generate_mapping(create_tables=True)


class DbHelper:
    """ Класс, реализующий методы для работы с локальной базой данных
    """

    def delete_all_markets(self):
        delete(market for market in Market)

    def save_markers(self, market):
        try:
            Market(vk_club_id=market['vk_club_id'],
                   audience=json.dumps(market['audience'], ensure_ascii=False)
                   if market['audience'] is not None else None,
                   name=market['name'],
                   description=market['description'],
                   activity=market['activity'],
                   photo_200=market['photo_200'],
                   contacts=json.dumps(market['contacts'], ensure_ascii=False)
                   if market['contacts'] is not None else None,
                   site=market['site'],
                   links=json.dumps(market['links'], ensure_ascii=False)
                   if market['links'] is not None else None,
                   total_wall_posts_count=market['total_wall_posts_count'],
                   wall_type=market['wall_type'],
                   country=market['country'],
                   city=market['city'],
                   last_post_date=market['last_post_date'],
                   products_count=market['products_count'],
                   median_price=market['median_price'],
                   average_price=market['average_price'],
                   products_example=json.dumps(market['products_example'], ensure_ascii=False)
                   if market['products_example'] is not None else None,
                   currency=market['currency'])

            logging.info('market with id = {} save in db'.format(market['vk_club_id']))

            commit()
        except Exception as e:
            logging.info('Problem save market {} save in db e: {}'.format(market, e))

    def get_markets_list(self):
        logging.info('get tokens')
        res = []
        for m in select(mark for mark in Market)[:]:
            m = m.to_dict()
            m['audience'] = json.loads(m['audience']) if m.get('audience') is not None else None
            m['contacts'] = json.loads(m['contacts']) if m.get('contacts') is not None else None
            m['links'] = json.loads(m['links']) if m.get('links') is not None else None
            m['products_example'] = json.loads(m['products_example']) if m.get('products_example') is not None else None
            res.append(m)

        return res

    def get_tokens(self):
        logging.info('get tokens')
        return select(tok for tok in Token)[:]

    def save_tokens(self, tokens_str_arr):
        logging.info('save tokens')
        for id, token in enumerate(tokens_str_arr):
            Token(token=token, id=id)

    def getTask(self):
        logging.info('get task')
        res = select(tok for tok in Task)[:1]
        return res[0] if len(res) > 0 else None

    def delete_task(self):
        delete(task for task in Task)

    def save_task(self, task_data):
        logging.info('save task')

        task = Task(id=task_data.get('id'), type=task_data.get('type'))

        if task.type == config.TYPE_SCAN_VK_MARKETS:
            task.vk_task_scan = VkTaskScan.create(task, task_data.get('task_obj').get('range').get('start'),
                                              task_data.get('task_obj').get('range').get('end'))

        if task.type == config.TYPE_UPDATE_VK_MARKETS:
            task.vk_task_update = VkTaskUpdate.create(task, json.dumps(task_data.get('task_obj').get('ids'),
                                                                       ensure_ascii=False))

    def get_token_id_by_token(self, token):
        pass

    def delete_token_by_id(self, token_id):
        pass
