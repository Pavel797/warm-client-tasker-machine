from multiprocessing import Queue
from creator.data_helper import DataHelper
from factory.task_process import TaskProcess
import logging
import sys
import time


class Worker:
    """Класс, реализующий основную логику работы 'исполнителя'
    """

    def __init__(self, api_key, base_url, _num_process):
        self.num_process = _num_process

        self.data_helper = DataHelper(api_key, base_url)

        self.procs = []
        self.progress_queues = []

    def work(self):
        """Метод обработки последователльной обработки задач
        """

        # для начала удалим все меющиеся результаты (вдруг что-то осталось в кеше)
        self.data_helper.delete_results()

        # получаем токены для работы
        tokens = self.data_helper.get_access_tokens(self.num_process)

        print('Start task!')

        # запоминаем время начала выполнения задачи
        time_start = time.time()

        # получаем общее кол-во элементов для отобрадения прогресса и список декомпозированных задач
        total_elements, tasks = self.data_helper.get_decompose_task(min(self.num_process, len(tokens)))

        # для каждой задачи создаём свой процесс
        for ind, task in enumerate(tasks):
            progress_queue = Queue()
            proc = TaskProcess(ind, self.data_helper, task, tokens[ind].token, progress_queue)
            self.progress_queues.append(progress_queue)
            self.procs.append(proc)
            proc.start()

        # отображем прогресс работы над задачей
        self.print_progress(total_elements)

        # на всякий случай ждём завершение терминальных операций в процессах и на всякий случай останавливаем их
        self.procs_join()
        self.stop()

        total_time = time.time() - time_start

        logging.info('Done task! time: {}'.format(total_time))
        print('Done task! time: {}'.format(total_time))

        # отправляем результат
        self.data_helper.send_task_result(total_time)

        # удаляем таску из кеша
        self.data_helper.delete_task()

    def stop(self):
        """Аварийное завершение всех запущенныз процессов
        """

        logging.info('Stop all tasks')
        for proc in self.procs:
            proc.terminate()

        for q in self.progress_queues:
            q.close()

        self.procs = []
        self.progress_queues = []

    def procs_join(self):
        """ Присоеденение всех дочерних процессов к родительскому
        """

        for proc in self.procs:
            try:
                proc.join()
            except Exception:
                logging.error('Process join error')

    def is_alive(self):
        """ Есть ли хотябы один процесс в работе
        :return: true or false
        """

        is_alive = False
        for proc in self.procs:
            is_alive = proc.is_alive()
            if is_alive:
                break

        return is_alive

    def print_progress(self, total_progress):
        """
        Отображение прогресса выполнения задачи
        :param total_progress:
        """

        save_progr = [0 for _ in range(0, len(self.procs))]

        while self.is_alive():

            progress_rev = 0
            for index_iter in range(0, len(self.procs)):
                if not self.progress_queues[index_iter].empty():
                    save_progr[index_iter] = self.progress_queues[index_iter].get()
                    progress_rev += save_progr[index_iter]
                else:
                    progress_rev += save_progr[index_iter]

            sys.stdout.write('\rprogers: {} / {}'.format(progress_rev, total_progress))

            # обновление прогреса каждые две секунды
            time.sleep(2)

        sys.stdout.write('\rprogers: {} / {}\n'.format(total_progress, total_progress))
