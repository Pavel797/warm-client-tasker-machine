from products.db_helper import DbHelper
import multiprocessing
import logging


class TaskProcess(multiprocessing.Process):
    """Класс процесса-обёртки для докомпозированных задач
    """

    def __init__(self, id, data_helper, task, access_token, progress_queue):
        multiprocessing.Process.__init__(self)
        self.progress_queue = progress_queue
        self.id = id
        self.access_token = access_token
        self.data_helper = data_helper
        self.task = task

        self.db = DbHelper()

    def run(self):
        logging.info('TaskProcess id {} type {} start'.format(self.id, self.task.type))
        self.task.work(self.task.data, self.access_token, self.data_helper, self.progress_queue)
        logging.info('TaskProcess id {} type {} stop'.format(self.id, self.task.type))
