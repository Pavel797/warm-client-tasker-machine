import json
import logging
import requests
import time
import config


class ApiHelper:
    """Класс для работы с API сервиса-заказчика
    """

    def __init__(self, api_key, base_url):
        self.headers = {'Authorization': 'Token {}'.format(api_key),
                        'Content-Type': 'application/json'}
        self.base_url = base_url
        logging.info('ApiHelper init base_url: {}, headers: {}'.format(base_url, self.headers))

    def get_vk_access_tokens(self, count):
        """Метод для получения списка токенов
        :param count: кол-во токенов
        :return list: список токенов
        """

        response = None
        answer = []
        while response is None or response.status_code != 200:
            if response is not None and response.status_code != 200:
                time.sleep(config.DELAY_ERROR_SERVER)

            try:
                response = requests.get("{}/social-tokens/vk/".format(self.base_url),
                                        params={'count': count},
                                        headers=self.headers)

                logging.info('Method \'get_vk_access_tokens\' count: {}'.format(count))
                answer = response.json()
            except Exception as e:
                logging.error(
                    '!ERROR! Method \'get_vk_access_tokens\' count: {}, response: {} exception: {}'.format(
                        count, response.content if response is not None else response, e))
                time.sleep(config.DELAY_ERROR_SERVER)

        return [token.get('token') for token in answer]

    def get_task(self):
        """Метод для получения задачи
        :return task: задача
        """
        response = None
        while response is None or response.status_code != 200:
            if response is not None and response.status_code != 200:
                time.sleep(config.DELAY_ERROR_SERVER)

            try:
                response = requests.get("{}/task/get/".format(self.base_url),
                                        headers=self.headers)
                logging.info('Method \'get_task\' response: {}'.format(response.content))
            except Exception as e:
                logging.error('!ERROR! Method \'get_task\' response: {} exception: {}'.format(
                    response.content if response is not None else response, e))
                time.sleep(config.DELAY_ERROR_SERVER)

        return response.json()

    def success_take_task(self, task_id):
        """Метод для подтверждения полученной задачи
        :param task_id: id взятой задачи
        :return информация о статусе запроса
        """

        response = None
        while response is None or (response.status_code != 200 and response.status_code != 208):
            try:
                response = requests.get("{}/task/confirm-receiving/{}/".format(self.base_url, task_id),
                                        headers=self.headers)
                logging.info(
                    'Method \'success_take_task\' task_id: {}'.format(task_id))
            except Exception as e:
                logging.error('!ERROR! Method \'success_take_task\' task_id: {}, response: {} exception: {}'.format(
                    task_id, response.content if response is not None else response, e))
                time.sleep(config.DELAY_ERROR_SERVER)

        return response.json()

    def success_take_tokens(self, tokens):
        print('success_take_tokens')
        # TODO: реализовать на сервере

    def send_task_result(self, markets, total_time, task_id):
        """Метод для отправки результатов по задаче
        :param total_time: общее время работы над задачей
        :param markets: информация о магазинах
        :param task_id: id задачи
        :return информация о статусе запроса
        """

        response = None
        while response is None or (response.status_code != 200 and response.status_code != 206):
            if response is not None and response.status_code != 200 and response.status_code != 206:
                time.sleep(config.DELAY_ERROR_SERVER)

            res = json.dumps({
                'task_id': task_id,
                'total_time': total_time,
                'result': markets
            })

            try:
                response = requests.post("{}/task/save-result/".format(self.base_url),
                                         data=res,
                                         headers=self.headers)
                logging.info('Method \'send_task_result\' total_time: {}, task_id: {}  response: {} status_code: {}'.format(
                    total_time, task_id, response.content, response.status_code))
            except Exception as e:
                logging.error(
                    '!ERROR! Method \'send_task_result\'total_time: {}, task_id: {},  response: {} exception: {} res: {}'.format(
                        total_time, task_id, response.content if response is not None else response, e, res))
                time.sleep(config.DELAY_ERROR_SERVER)

        return response

    def mark_token_as_invalid(self, token):
        """Метод для пометки токена как невалидный
        :param token: невалидный токен
        :return информация о статусе запроса
        """

        response = None
        while response is None or (response.status_code != 200 and response.status_code != 206):
            try:
                response = requests.get("{}social-tokens/delete/{}/".format(self.base_url, token),
                                        headers=self.headers)
                logging.info(
                    'Method \'mark_token_as_invalid\' token: {} response: {}'.format(token, response.content))
            except Exception as e:
                logging.error('!ERROR! Method \'send_task_result\' token: {} response: {} exception: {}'.format(
                    token, response.content if response is not None else response, e))
                time.sleep(config.DELAY_ERROR_SERVER)

        return response.json()

    def get_one_vk_access_token(self):
        """
        Метод для получение одного токена, такая ситуация возникает при замене невалидного токена
        :return: один токен
        """

        logging.info('Method \'get_one_vk_access_token\'')
        return self.get_vk_access_tokens(1)[0]
