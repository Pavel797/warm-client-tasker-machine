import logging


def get_field_or_none(obj, field):
    """ Получение данных по ключу из словаря, если они есть. Иначе возвращаем заглушку, означающую
    отстутствие данных (не обязательно None)

    :param obj:
    :param field:
    :return:
    """
    return obj.get(field) if obj.get(field) else None


def load_settings(file_name='settings.txt'):
    """ Загрузка настроек из файла
    :param file_name:
    :return: словарь с настройками вида ключ-значение
    """
    file = open(file_name)
    lines = [line.split() for line in file]

    for line in lines:
        if len(line) != 2:
            raise IOError("Settings file invalid format")

    return {line[0]: line[1] for line in lines}


def init_logger():
    """ Инициализация библиотеки логирования
    """
    logging.basicConfig(level=logging.INFO,
                        filename="machine-log.log",
                        format='[%(asctime)s] - %(levelname)-8s - %(filename)s - %(message)s')
