import numpy as np


class ProductsAnalyzer:
    """ Класс, реализующий анализ продуктов:
    1) Лист с примерами 3х продуктов:
            a) самый дешёвый
            б) медианный по цене
            в) самый дорогой
    2) Общее кол-во продуктов
    3) Медианная цена
    4) Среднеарифметическая цена
    5) Валюта
    """

    def __init__(self, data_aray):
        if data_aray is None or data_aray.size == 0:
            self.products_example = None
            self.products_count = None
            self.median_price = None
            self.average_price = None
            self.currency = None
        else:
            self.products_count = data_aray.size
            self.currency = data_aray[0].get('currency')

            sorted(data_aray, key=lambda product: product.get('amount'))

            self.median_price = data_aray[data_aray.size // 2].get('amount')
            self.products_example = [data_aray[0], data_aray[data_aray.size // 2], data_aray[data_aray.size - 1]]

            self.average_price = 0
            for product in data_aray:
                self.average_price += (product.get('amount') / data_aray.size)
            self.average_price = int(self.average_price)

    def valid(self):
        return True if self.products_example is not None else False
