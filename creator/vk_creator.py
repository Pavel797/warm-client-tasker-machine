from creator.products_analyzer import ProductsAnalyzer
from vk_api.exceptions import VkApiError
from vk_api.execute import VkFunction
from utils import get_field_or_none
from exceptions.vk_token_invalid import VkTokenInvalid
import numpy as np
import logging
import vk_api
import time
import re
import numpy as np


def get_product_data(product):
    """ Метод для создания модели продукта в виде словаря
    :param product:
    :return: словарь-модель продукта
    """
    return {
        'title': product.get('title'),
        'description': product.get('description'),
        'amount': int(product.get('price').get('amount')),
        'thumb_photo': product.get('thumb_photo'),
        'currency': product.get('price', {'currency': None}).get('currency', {'name': None}).get('name')
    }


class VkMarketsCreator:
    """ Класс, реализующий обработку задачи в социальной сети Вконтакте по анализу маркетов
    """

    FIELDS = 'type,is_closed,deactivated,photo_100,photo_200,activity,city,' \
             'contacts,country,description,ban_info,links,markert,place,site,' \
             'start_date,finish_date,wall,market'

    def __init__(self, access_token):
        self.logger = logging.getLogger("VkApiHelper")
        self.vk_session = vk_api.VkApi(token=access_token)
        self.vk = self.vk_session.get_api()

    def scan_markets_range_ids(self, start_id, end_id):
        """ Метод, сканирующий диапазон маркетов и отсекающий невалидные
        :param start_id:
        :param end_id:
        :return: лист ids валидных маркетов
        """

        start_id, end_id = (end_id, start_id) if start_id > end_id else (start_id, end_id)

        result = []
        for i, id in enumerate(range(start_id, end_id + 1, 500)):
            _range = str([_id for _id in range(id, id + 500)])
            _range = re.sub(r'\[|\]| ', '', _range)
            result.append(_range)

        if len(result) % 25 != 0:
            for _ in range(25 - (len(result) % 25)):
                result.append(None)

        valid_market_ids = []

        for ind in range(0, len(result), 25):
            try:
                markets_list = self.custom_executor(scan_groups_ids,
                                                    result[ind], result[ind + 1], result[ind + 2], result[ind + 3],
                                                    result[ind + 4], result[ind + 5], result[ind + 6], result[ind + 7],
                                                    result[ind + 8], result[ind + 9], result[ind + 10],
                                                    result[ind + 11], result[ind + 12], result[ind + 13],
                                                    result[ind + 14], result[ind + 15], result[ind + 16],
                                                    result[ind + 17], result[ind + 18], result[ind + 19],
                                                    result[ind + 20], result[ind + 21], result[ind + 22],
                                                    result[ind + 23], result[ind + 24])

                for market_range in markets_list:
                    for ind in range(len(market_range.get('ids', []))):
                        if market_range.get('is_enabled')[ind] and not market_range.get('is_closed')[ind]:
                            valid_market_ids.append(market_range.get('ids')[ind])
            except Exception as e:
                self.logger.error("Problem with range start_id = {} end_id = {} e = {}".format(start_id, end_id, e))
                return None

        return valid_market_ids

    def get_markets_list_by_id(self, vk_markets_id):
        """ Метод, собирающий и анализирующий интересующую нас информацию о конкретном маркете
        :param vk_markets_id:
        :return: словарь-модель каркета
        """

        try:
            group_data = self.custom_executor(pars_group, vk_markets_id, self.FIELDS)
        except Exception as e:
            self.logger.error("Problem with id = {} e = {}".format(vk_markets_id, e))
            return None

        # нас не интересуют закрытые группы
        if group_data.get('is_closed') or not group_data.get('market') or \
                not group_data.get('market').get('enabled'):
            return None

        # анализ продуктов маркета
        products_anal = self.__get_products_info__(group_data.get('id'))

        # если двнные о продуктах невалидны, то нас этот маркет не интересует
        if not products_anal.valid():
            return None

        market = dict()
        market['vk_club_id'] = group_data.get('id')
        market['audience'] = group_data.get('audience')
        market['name'] = get_field_or_none(group_data, 'name')
        market['description'] = get_field_or_none(group_data, 'description')
        market['activity'] = get_field_or_none(group_data, 'activity')
        market['photo_200'] = get_field_or_none(group_data, 'photo_200')
        market['contacts'] = get_field_or_none(group_data, 'contacts')
        market['site'] = get_field_or_none(group_data, 'site')
        market['links'] = get_field_or_none(group_data, 'links')
        market['total_wall_posts_count'] = get_field_or_none(group_data, 'total_wall_posts_count')
        market['wall_type'] = get_field_or_none(group_data, 'wall')
        market['country'] = group_data.get('country', {'title': None}).get('title')
        market['city'] = group_data.get('city', {'title': None}).get('title')
        market['last_post_date'] = group_data.get('last_post_date')

        market['products_count'] = products_anal.products_count
        market['median_price'] = products_anal.median_price
        market['average_price'] = products_anal.average_price
        market['products_example'] = products_anal.products_example
        market['currency'] = products_anal.currency

        return market

    def __get_products_info__(self, market_id):
        market_array = np.array([])
        try:
            ans = self.base_executor(self.vk.market.get, owner_id=-market_id, count=200)

            if ans is None:
                return ProductsAnalyzer(None)

            count_all = ans.get('count')
            market_array = np.append(market_array, [get_product_data(product) for product in ans.get('items')])

            while market_array.size < count_all:
                ans = self.base_executor(self.vk.market.get, owner_id=-market_id, count=200, offset=market_array.size)
                if ans is None or len(ans.get('items')) == 0:
                    return ProductsAnalyzer(market_array)

                market_array = np.append(market_array, [get_product_data(product) for product in ans.get('items')])

            return ProductsAnalyzer(market_array)
        except vk_api.exceptions.ApiError as e:
            return ProductsAnalyzer(None)

    def custom_executor(self, method, *params):
        data = None
        while not data:
            try:
                data = method(self.vk, *params)
            except VkApiError as e:
                if hasattr(e, 'code'):
                    if e.code == 15:
                        # self.logger.error("VkApiError exceptions: {}".format(e))
                        return None
                    elif e.code == 4 or e.code == 5:
                        print('Token invalid!!!')
                        self.logger.error("Token {} invalid!!!".format(self.vk.token))
                        raise VkTokenInvalid(self.vk_session.token)
                    else:
                        print('VkApiError', e)
                        self.logger.error("VkApiError exceptions: {}".format(e))
                        return None
                else:
                    self.logger.error("Exception VkApiError for method '{}' params {}  exceptions: {}"
                                      .format(method, params, e))
                    time.sleep(60)
            except Exception as e:
                self.logger.error("Exception custom_executor for method '{}' params {}  exceptions: {}"
                                  .format(method, params, e))
                time.sleep(60)
        return data

    def base_executor(self, method, **params):
        data = None
        while not data:
            try:
                data = method(**params)
            except VkApiError as e:
                if hasattr(e, 'code'):
                    if e.code == 15:
                        # self.logger.error("VkApiError exceptions: {}".format(e))
                        return None
                    elif e.code == 4 or e.code == 5:
                        print('Token invalid!!!')
                        self.logger.error("Token {} invalid!!!".format(self.vk.token))
                        raise VkTokenInvalid(self.vk_session.token)
                    else:
                        print('VkApiError', e)
                        self.logger.error("VkApiError exceptions: {}".format(e))
                        return None
                else:
                    self.logger.error("Exception VkApiError for method '{}' params {}  exceptions: {}"
                                      .format(method, params, e))
                    time.sleep(60)
            except Exception as e:
                self.logger.error("Exception base_executor for method '{}' params {} exceptions: {}"
                                  .format(method, params, e))
                time.sleep(60)
        return data


# Удалённая процедура для получения интересующих нас данных о конкретной группе, коорая делает 5 запросов
# к API vk в рамках одного
pars_group = VkFunction(args=('group_id', 'fields'), code='''
var groupId = %(group_id)s;
var fields = %(fields)s;

var group = API.groups.getById({"group_id": groupId, "fields": fields})[0];

group.last_post_date = null;

var lastPosts = API.wall.get({"owner_id":-groupId, "count":2});
group.total_wall_posts_count = lastPosts.count;

var iterIndex = 0;
while (iterIndex < lastPosts.items.length) {
    if (group.last_post_date == null || 
        group.last_post_date < lastPosts.items[iterIndex].date)
        group.last_post_date = lastPosts.items[iterIndex].date;
    iterIndex = iterIndex + 1;
}

group.audience = {
    "total": API.users.search({"group_id": groupId, "count": 0, "sex": 0}).count,
    "men": API.users.search({"group_id": groupId, "count": 0, "sex": 2}).count,
    "women": API.users.search({"group_id": groupId, "count": 0, "sex": 1}).count
};

return group;
''')

# Удалённая процедура для быстрого сканирования до 25 диапазонов групп по 500 элементов
scan_groups_ids = VkFunction(args=('range1', 'range2', 'range3', 'range4', 'range5',
                                   'range6', 'range7', 'range8', 'range9', 'range10',
                                   'range11', 'range12', 'range13', 'range14', 'range15',
                                   'range16', 'range17', 'range18', 'range19', 'range20',
                                   'range21', 'range22', 'range23', 'range24', 'range25'),
                             code='''
var range1 = %(range1)s;
var range2 = %(range2)s;
var range3 = %(range3)s;
var range4 = %(range4)s;
var range5 = %(range5)s;
var range6 = %(range6)s;
var range7 = %(range7)s;
var range8 = %(range8)s;
var range9 = %(range9)s;
var range10 = %(range10)s;
var range11 = %(range11)s;
var range12 = %(range12)s;
var range13 = %(range13)s;
var range14 = %(range14)s;
var range15 = %(range15)s;
var range16 = %(range16)s;
var range17 = %(range17)s;
var range18 = %(range18)s;
var range19 = %(range19)s;
var range20 = %(range20)s;
var range21 = %(range21)s;
var range22 = %(range22)s;
var range23 = %(range23)s;
var range24 = %(range24)s;
var range25 = %(range25)s;

var args = [range1, range2, range3, range4, range5,
    range6, range7, range8, range9, range10, 
    range11, range12, range13, range14, range15,
    range16, range17, range18, range19, range20,
    range21, range22, range23, range24, range25];

var ans = [];

var i = 0;
while (i < args.length) {
    if (args[i] != null) {
        var resGroups = API.groups.getById({"group_ids": args[i],
        "fields":"market"});
        
        ans.push({
            ids:  resGroups@.id,
            is_enabled:  resGroups@.market@.enabled,
            is_closed:  resGroups@.market@.is_closed
        });
    }
    i = i + 1;
}

return ans; 
''')
