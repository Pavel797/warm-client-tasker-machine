from api.api_helper import ApiHelper
from multiprocessing import Lock
from products.db_helper import DbHelper
import logging
import time
from pony.orm import db_session


class DataHelper:
    """Клас, реазующий интерфейс абстрагированной работы с
    источниками данных
    """

    def __init__(self, api_key, base_url):

        self.lock = Lock()
        self.api_helper = ApiHelper(api_key, base_url)
        self.db_helper = DbHelper()

    @db_session
    def get_access_tokens(self, number_of_tokens):
        """Получение токенов от сервиса-заказчика
        после получения токены кешируются в локальную БД

        :param number_of_tokens:
        :return лист моделей токенов:
        """
        tokens = self.__get_locale_tokens__()

        if len(tokens) < number_of_tokens:
            num_getting_tokens = number_of_tokens - len(tokens)
            logging.info('Get {} access tokens'.format(num_getting_tokens))

            get_tokens = self.api_helper.get_vk_access_tokens(num_getting_tokens)
            self.api_helper.success_take_tokens(get_tokens)
            self.db_helper.save_tokens(get_tokens)

            tokens = self.__get_locale_tokens__()

        return tokens

    @db_session
    def delete_results(self):
        """Удаление сохранённых результатов предыдущей задачи
        """
        self.db_helper.delete_all_markets()

    @db_session
    def __get_locale_tokens__(self):
        """Загрузка токенов из кеша
        :return лист моделей токенов:
        """

        logging.info('Load local tokens...')
        result = self.db_helper.get_tokens()
        logging.info('Loaded {} local tokens'.format(len(result)))
        return result

    @db_session
    def get_decompose_task(self, num_tasks):
        """Получение из декомпозированеи задач
        :return лист моделей декомпозированных задач:
        """

        task = self.__get_locale_task__()

        if task:
            return task.decompose(num_tasks)

        logging.info('Get task')

        count_attempt = 1
        while not task:

            if count_attempt > 1:
                time.sleep(60)
                logging.info('Get task attempt num {}'.format(count_attempt))

            get_task = self.api_helper.get_task()
            self.db_helper.save_task(get_task)

            task = self.__get_locale_task__()
            logging.info('Given the task {}'.format(str(task)))

            count_attempt += 1

        self.api_helper.success_take_task(task.id)

        return task.decompose(num_tasks)

    @db_session
    def __get_locale_task__(self):
        """Загрузка задачи из кеша
        :return модель задачи:
        """

        logging.info('Load local task...')
        task = self.db_helper.getTask()
        logging.info('Loaded {} local task'.format(task))
        return task if task else None

    @db_session
    def send_task_result(self, total_time):
        """Отправка результатов задачи на сервер
        :param total_time:
        """

        task = self.db_helper.getTask()
        if task is None:
            logging.error('Can not send result!!!')
            print('Can not send result!!!')
        else:
            self.api_helper.send_task_result(self.db_helper.get_markets_list(), round(total_time, 2),
                                             task.id)

    @db_session
    def delete_task(self):
        """Удаление кешированной задачи из БД
        """
        self.db_helper.delete_task()

    @db_session
    def save_markers(self, market):
        """Сохранение маркетов БД
        :param market:
        """
        with self.lock:
            self.db_helper.save_markers(market)

    @db_session
    def mark_token_as_invalid_and_update(self, token):
        """Удаление из кеша невалидного токена и замена его на новый
        :param token:
        :return: новый токен
        """
        token_id = self.db_helper.get_token_id_by_token(token)
        self.db_helper.delete_token_by_id(token_id)

        self.api_helper.mark_token_as_invalid(token)
        token = self.api_helper.get_one_vk_access_token()
        self.db_helper.save_tokens(token)
        return token
