from factory.worker import Worker
import logging
import utils
import time
from products.db_helper import db_init


def main():
    utils.init_logger()

    logging.info('Start load settings')
    settings = utils.load_settings()
    logging.info('End load settings')

    db_init()

    worker = Worker(settings['key'], settings['base_url'], int(settings['num_process']))

    logging.info('Set base url {}'.format(settings['base_url']))

    print('Start work!')
    logging.info('Start work')

    # два try-except для того чтобы дочерние процессы не плевались в консоль исключениями
    try:
        # основной цикл работы исполнителя (если исключение - пробуем снова)
        while True:
            try:
                worker.work()
            except Exception as e:
                if e is KeyboardInterrupt or e is SystemExit:
                    raise e

                worker.stop()
                logging.exception("Error!")
                time.sleep(60)
                logging.info("Restart...")

                worker = Worker(settings['key'], settings['base_url'], int(settings['num_process']))

    except (KeyboardInterrupt, SystemExit):
        worker.stop()
        print("Exit")
        logging.info("Exit!")


if __name__ == "__main__":
    main()

    #from api.api_helper import ApiHelper

    #print(ApiHelper('hui-pizda-i-zhopa123', 'http://157.230.96.184/api/v1').send_task_result([], 12, 1))

    """
    from creator.vk_creator import VkMarketsCreator
    import json


    vk = VkMarketsCreator('f7e8c6baa98e14a81f75a3e35a06b5d366a8633e4b9a8848d41cc18b18e64ff6e6a08897e5ec908c6971c')

    print(vk.get_markets_list_by_id(9476287))
    """

    """
    t = time.time()
    print([len() for i in range(1, 10000, 10000)])
    print(time.time() - t)
    """

    """
    from vkapihelper import VkApiHelper
    import json

    vk_helper = VkApiHelper('68426fa8d7466cd656dfe04078fd4cc4c48a097ad6a7e496a38a70783f09a269447018b7a8633097d1ba0')
    step = 499
    start = 146727472
    stop =  246727472

    for i in range(start, stop, step):
        print(json.dumps(vk_helper.get_markers_list_by_ids([id for id in range(i, i + step + 1)]).tolist(),
                         ensure_ascii=False))
    """
